---
layout: redirect
title: Redirect
description: ''
permalink: /blacktoon
lang: ko
lang_index: 0
meta:
  title: 블랙툰
  desc: 가장 많은 무료웹툰을 보유하고 있으며 빠른 업데이트와 편의성으로 네이버웹툰, 다음웹툰, 카카오웹툰, 레진코믹스 웹툰 서비스 제공
  link: 'https://olink.netlify.app/webtoon/blacktoon'
  img: '/assets/images/blacktoon-link.jpg'
  domain: blacktoon.com
---
