---
layout: redirect
title: Redirect
description: ''
permalink: /pro
lang: ko
lang_index: 0
meta:
  title: 프로툰
  desc: 웹툰 미리보기 No.1 프로툰, 프로툰에서 최신 웹툰을 찾아보세요!
  link: 'https://olink.netlify.app/webtoon/pro'
  img: '/assets/images/pro-link.jpg'
  domain: protoon.com
---
